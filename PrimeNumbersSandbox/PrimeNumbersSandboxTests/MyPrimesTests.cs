﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using PrimeNumbersSandbox;
using System.Linq;

namespace PrimeNumbersSandboxTests
{
    //public class MyPrimesTests
    //{
    //    private readonly int[] _primesUptoThirtyOne =
    //        { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31 };

    //    #region positive 

    //    [Fact]
    //    public void IsPrime_ShouldReturnTrue_WhenNumberIsTwo()
    //    {
    //        var result = MyPrimesWithIntArray.IsPrime(2);
    //        Assert.True(result);
    //    }


    //    [Fact]
    //    public void IsMersennePrime_ShouldReturnTrue_WhenPowerOfTwoIsPrime()
    //    {
    //        var mersennePrimes = _primesUptoThirtyOne
    //            .Take(5)
    //            .Select(x => Math.Pow(2, x) - 1)
    //            .Select(x => (int)x);

    //        var result = mersennePrimes.All(x => MyPrimesWithIntArray.IsMersennePrime(x));
    //        Assert.True(result);
    //    }

    //    [Fact]
    //    public void GetPrimes_ShouldReturnBenchmark_WhenGivenOnlyCeiling()
    //    {
    //        var result = MyPrimesWithIntArray.GetPrimes(32);

    //        Assert.Equal(result, _primesUptoThirtyOne);
    //    }

    //    [Fact]
    //    public void GetPrimes_ShouldReturnSingleElement_WhenFloorEqualsCeiling_Prime()
    //    {
    //        foreach (var prime in _primesUptoThirtyOne)
    //        {
    //            var result = MyPrimesWithIntArray.GetPrimes(prime, prime);

    //            Assert.Single(result);
    //            Assert.Equal(result.First(), prime);
    //        }
    //    }

    //    [Fact]
    //    public void GetPrimes_ShouldReturnEmptyList_WhenFloorEqualsCeiling_Composite()
    //    {
    //        var evens = Enumerable.Range(2, 10).Select(x => x * 2).ToList();

    //        foreach (var even in evens)
    //        {
    //            var result = MyPrimesWithIntArray.GetPrimes(even, even);

    //            Assert.Empty(result);
    //        }
    //    }

    //    #endregion

    //    #region negative 

    //    [Fact]
    //    public void IsPrime_ShouldReturnFalse_WhenNumberIsZero()
    //    {
    //        var result = MyPrimesWithIntArray.IsPrime(0);
    //        Assert.False(result);
    //    }

    //    [Fact]
    //    public void IsPrime_ShouldReturnFalse_WhenNumberIsOne()
    //    {
    //        var result = MyPrimesWithIntArray.IsPrime(1);
    //        Assert.False(result);
    //    }

    //    [Fact]
    //    public void IsPrime_ShouldReturnFalse_WhenNumberIsNegativeOne()
    //    {
    //        var result = MyPrimesWithIntArray.IsPrime(-1);
    //        Assert.False(result);
    //    }

    //    [Fact]
    //    public void IsPrime_ShouldReturnFalse_WhenNumberIsEven()
    //    {
    //        var evens = Enumerable.Range(2, 10).Select(x => x * 2).ToList();

    //        var result = evens.All(x => !MyPrimesWithIntArray.IsPrime(x));
    //        Assert.True(result);
    //    }

    //    [Fact]
    //    public void IsPrime_ShouldReturnFalse_WhenNumberIsMultipleOfThree()
    //    {
    //        var evens = Enumerable.Range(2, 10).Select(x => x * 3).ToList();

    //        var result = evens.All(x => !MyPrimesWithIntArray.IsPrime(x));
    //        Assert.True(result);
    //    }


    //    [Fact]
    //    public void IsPrime_ShouldReturnFalse_WhenNumberIsPerfectSquare()
    //    {
    //        var evens = Enumerable.Range(2, 10).Select(x => x * x);

    //        var result = evens.All(x => !MyPrimesWithIntArray.IsPrime(x));
    //        Assert.True(result);
    //    }

    //    [Fact]
    //    public void IsPrime_ShouldReturnFalse_WhenNumberIsNegative()
    //    {
    //        var negatives = _primesUptoThirtyOne.Select(x => x * (-1));

    //        var result = negatives.All(x => !MyPrimesWithIntArray.IsPrime(x));
    //        Assert.True(result);
    //    }

    //    [Fact]
    //    public void IsPrime_ShouldReturnFalse_WhenNumberIsProduct()
    //    {
    //        var random = new Random();
    //        var products = _primesUptoThirtyOne.Select(x => x * random.Next());

    //        var result = products.All(x => !MyPrimesWithIntArray.IsPrime(x));
    //        Assert.True(result);
    //    }

    //    [Fact]
    //    public void IsMersennePrime_ShouldReturnFalse_WhenPowerOfTwoIsntPrime()
    //    {
    //        var mersenneNotPrimes = Enumerable.Range(4, 5)
    //            .Select(x => x * 2)
    //            .Select(x => Math.Pow(2, x) - 1)
    //            .Select(x => (int)x);

    //        var result = mersenneNotPrimes.All(x => !MyPrimesWithIntArray.IsMersennePrime(x));
    //        Assert.True(result);
    //    }

    //    [Fact]
    //    public void IsMersennePrime_ShouldReturnFalse_WhenPrimeButNotMersenne()
    //    {
    //        var mersennePrimes = _primesUptoThirtyOne
    //            .Take(5)
    //            .Select(x => Math.Pow(2, x) - 1)
    //            .Select(x => (int)x);

    //        var notMersennePrimes = _primesUptoThirtyOne.Except(mersennePrimes);

    //        var result = notMersennePrimes.All(x => !MyPrimesWithIntArray.IsMersennePrime(x));
    //        Assert.True(result);
    //    }

    //    [Fact]
    //    public void GetPrimes_ShouldThrowException_WhenCeilingLessThanFloor()
    //    {
    //        Action result = () => MyPrimesWithIntArray.GetPrimes(100, 5);
    //        var exception = Record.Exception(result);

    //        Assert.IsType<ArgumentException>(exception);
    //        Assert.Contains(exception.Message, "Ceiling can't be less than floor!");
    //    }

    //    #endregion
    //}
}
