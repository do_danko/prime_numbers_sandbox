﻿using PrimeNumbersSandbox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace PrimeNumbersSandboxTests
{
    public class EratosthenesSieveTest
    {
        #region fields

        private readonly ulong[] _primesUpto100 =
            {
                2, 3, 5, 7, 11, 13, 17, 19, 23, 29
                , 31, 37, 41, 43, 47, 53, 59, 61, 67, 71
                , 73, 79, 83, 89, 97
            };

        private const int _intCeiling = 100;

        #endregion

        #region positive

        [Fact]
        public void GetPrimes_ShouldReturnBenchmark()
        {
            var sieve = new EratosthenesSieve();
            var result = sieve.GetPrimes(_intCeiling);
            result.Sort();

            Assert.Equal(result, _primesUpto100);
        }

        #endregion

        #region negative
        #endregion
    }
}
