﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrimeNumbersSandbox
{
    public static class MyPrimesWithIntArray
    {
        #region fields

        private const int MIN_PRIME = 2;
        private const int SIEVE_SEGMENT_LENGTH = 10;

        private static HashSet<int> _cachedPrimes;
        private static int _maxCheckedOnPrimality;

        #endregion

        #region ctor

        static MyPrimesWithIntArray()
        {
            _cachedPrimes = new HashSet<int>();
            _maxCheckedOnPrimality = MIN_PRIME;
        }

        #endregion

        #region public methods

        public static bool IsPrime(int value)
        {
            bool result;

            if (value < MIN_PRIME)
            {
                result = false;
            }
            else if (_cachedPrimes.Contains(value))
            {
                result = true;
            }
            else
            {
                var isPrime = IsPrimeByTrialDivision(value);
                if (isPrime)
                {
                    _cachedPrimes.Add(value);
                }

                result = isPrime;
            }

            return result;
        }

        // Mersenne prime: (2**p - 1) where p is prime
        public static bool IsMersennePrime(int value)
        {
            var result = false;

            if (MIN_PRIME <= value)
            {
                var logFromValuePlusOne = Math.Log(value + 1, 2);
                if (logFromValuePlusOne % 1 == 0)
                {
                    result = IsPrime((int) logFromValuePlusOne);
                }
            }

            return result;
        }

        public static List<int> GetPrimes(int ceiling)
        {
            List<int> result;

            if (ceiling < 1)
            {
                result = new List<int>();
            }
            else if (ceiling <= _maxCheckedOnPrimality)
            {
                result = _cachedPrimes.Where(x => x < ceiling).ToList();
            }
            else
            {
                result = GetPrimesBySieveWithLessThanSqrt(ceiling);

                var valuesToCache = result.Where(x => _maxCheckedOnPrimality < x);

                _cachedPrimes.AddRange(valuesToCache);
                _maxCheckedOnPrimality = ceiling;
            }

            return result;
        }

        public static List<int> GetPrimes(int floor, int ceiling)
        {
            List<int> result;

            if (ceiling < floor)
            {
                throw new ArgumentException("Ceiling can't be less than floor!");
            }

            if (ceiling < 1)
            {
                result = new List<int>();
            }
            else if (floor == ceiling)
            {
                result = new List<int>();

                var isPrime = IsPrime(floor);
                if (isPrime)
                {
                    result.Add(floor);
                }
            }
            else
            {
                var primesFromMinToCeiling = GetPrimesByEratosthenesSieve(ceiling);

                result = primesFromMinToCeiling.Where(x => floor <= x).ToList();
            }
            
            return result;
        }

        #endregion

        #region private methods

        private static List<int> GetPrimesByEratosthenesSieve(int ceiling)
        {
            var possibleDivisors = Enumerable.Range(0, ceiling)
                .SkipWhile(x => x < MIN_PRIME)
                .Select(x => x as int?)
                .ToArray();
            
            for (int i = 0; i < possibleDivisors.Length; ++i)
            {
                if (possibleDivisors[i] == null)
                {
                    continue;
                }

                var step = possibleDivisors[i].Value;

                for (int j = i + step; j < possibleDivisors.Length; j += step)
                {
                    possibleDivisors[j] = null;
                }
            }

            var result = possibleDivisors.Where(x => x.HasValue)
                .Select(x => x.Value)
                .ToList();

            return result;
        }

        private static List<int> GetPrimesBySieveWithLessThanSqrt(int ceiling)
        {
            var valueSqrt = Convert.ToInt32(Math.Ceiling(Math.Sqrt(ceiling)));
            var possibleDivisors = Enumerable.Range(0, ceiling)
                .SkipWhile(x => x < MIN_PRIME)
                .Select(x => x as int?)
                .ToArray();

            for (int i = 0; i < possibleDivisors.Length; ++i)
            {
                if (possibleDivisors[i] == null)
                {
                    continue;
                }

                if (valueSqrt < possibleDivisors[i])
                {
                    break;
                }

                var step = possibleDivisors[i].Value;

                for (int j = i + step; j < possibleDivisors.Length; j += step)
                {
                    possibleDivisors[j] = null;
                }
            }

            var result = possibleDivisors.Where(x => x.HasValue)
                .Select(x => x.Value)
                .ToList();

            return result;
        }

        private static List<int> GetPrimesBySegmentedSieve(int ceiling)
        {
            throw new NotImplementedException();
        }

        private static bool IsPrimeByTrialDivision(int value)
        {
            var result = true;

            var valueSqrt = Math.Sqrt(value);
            var isPerfectSqrt = valueSqrt % 1 == 0;
            if (isPerfectSqrt)
            {
                result = false;
            }
            else
            {
                var possibleDivisors = Enumerable
                    .Range(0, Convert.ToInt32(Math.Ceiling(valueSqrt)))
                .SkipWhile(x => x < MIN_PRIME)
                .ToArray();

                foreach (var possibleDivisor in possibleDivisors)
                {
                    if (value % possibleDivisor == 0)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        #endregion
    }
}
