﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrimeNumbersSandbox
{
    public static class MyPrimesWithBoolArray
    {
        #region fields

        private const int MIN_PRIME = 2;
        private const int SIEVE_SEGMENT_LENGTH = 10;

        #endregion

        #region public methods

        public static List<int> GetPrimesByEratosthenesSieve(int ceiling)
        {
            var primes = new bool[ceiling];

            primes.Populate(true);
            primes[0] = false;
            primes[1] = false;

            for (int i = MIN_PRIME; i < ceiling; ++i)
            {
                for (int j = i + i; j < ceiling; j += i)
                {
                    primes[j] = false;
                }
            }

            var result = primes
                .Select((IsPrime, Index) => new { IsPrime, Index })
                .Where(x => x.IsPrime)
                .Select(x => x.Index)
                .ToList();

            return result;
        }

        public static List<int> GetPrimesBySieveWithLessThanSqrt(int ceiling)
        {
            var primes = new bool[ceiling];

            primes.Populate(true);
            primes[0] = false;
            primes[1] = false;

            for (int i = MIN_PRIME; i * i < ceiling; ++i)
            {
                for (int j = i + i; j < ceiling; j += i)
                {
                    primes[j] = false;
                }
            }

            var result = primes
                .Select((IsPrime, Index) => new { IsPrime, Index })
                .Where(x => x.IsPrime)
                .Select(x => x.Index)
                .ToList();

            return result;
        }

        public static List<int> GetPrimesBySievingOnlyOdds(int ceiling)
        {
            var newCeiling = Convert.ToInt32(Math.Ceiling(ceiling / 2.0));
            var primes = new bool[newCeiling];
            primes.Populate(true);
            primes[0] = false;

            for (int i = 1; i < newCeiling; ++i)
            {
                var step = 2 * i + 1;
                for (int j = i + step; j < newCeiling; j += step)
                {
                    primes[j] = false;
                }
            }

            var result = primes
                .Select((IsPrime, Index) => new { IsPrime, Index })
                .Where(x => x.IsPrime)
                .Select(x => 2 * x.Index + 1)
                .ToList();

            result.Insert(0, 2);

            return result;
        }

        public static List<int> GetPrimesByAtkinSieve(int ceiling)
        {
            var primes = new bool[ceiling];
            var ceilingSqrt = Math.Sqrt(ceiling);

            for (int x = 1; x <= ceilingSqrt; ++x)
            {
                for (int y = 1; y <= ceilingSqrt; ++y)
                {
                    var x2 = x * x;
                    var y2 = y * y;

                    var n = 4 * x2 + y2;
                    if (ceiling < n)
                    {
                        continue;
                    }

                    if (n % 12 == 1 || n % 12 == 5)
                    {
                        primes[n] = true;
                    }

                    n = n - x2;
                    if (n % 12 == 7)
                    {
                        primes[n] = true;
                    }

                    n = n - 2 * y2;
                    if (y < x && n % 12 == 11)
                    {
                        primes[n] = true;
                    }
                }
            }

            for (int i = 1; i <= ceilingSqrt; i += 2)
            {
                if (primes[i])
                {
                    var square = i * i;
                    for (int j = square; j < ceiling; j += square)
                    {
                        primes[j] = false;
                    }
                }
            }

            primes[2] = true;
            primes[3] = true;

            var result = primes
                .Select((IsPrime, Index) => new { IsPrime, Index })
                .Where(x => x.IsPrime)
                .Select(x => x.Index)
                .ToList();

            return result;
        }

        #endregion
    }
}
