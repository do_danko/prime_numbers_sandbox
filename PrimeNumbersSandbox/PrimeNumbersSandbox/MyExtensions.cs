﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrimeNumbersSandbox
{
    public static class MyExtensions
    {
        public static void AddRange<T>(this HashSet<T> set, IEnumerable<T> items)
        {
            if (set == null || items == null)
            {
                throw new ArgumentException();
            }

            foreach (var item in items)
            {
                set.Add(item);
            }
        }

        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> items, int chunkLength)
        {
            if (items == null || chunkLength < 1)
            {
                throw new ArgumentException();
            }

            var result = items
                .Select((value, index) => new { Value = value, Index = index })
                .GroupBy(x => x.Index / chunkLength)
                .Select(x => x.Select(y => y.Value));

            return result;
        }

        public static void Populate<T>(this T[] array, T defaultValue)
        {
            if (array == null)
            {
                throw new ArgumentException();
            }

            for (int i = 0; i < array.Length; ++i)
            {
                array[i] = defaultValue;
            }
        }

        public static IEnumerable<int> GetIndexes<T>(this T[] items, Predicate<T> condition)
        {
            if (items == null || condition == null)
            {
                throw new ArgumentException();
            }

            for (int i = 0; i < items.Length; ++i)
            {
                if (condition(items[i]))
                {
                    yield return i;
                }
            }
        }
    }
}
