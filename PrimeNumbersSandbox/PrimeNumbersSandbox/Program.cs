﻿using System;
using System.Diagnostics;
using System.Linq;

namespace PrimeNumbersSandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            var diagnostics = new DiagnosticsTool();

            Console.WriteLine("EratosthenesSieve");
            var eratosthenesBasic = new EratosthenesSieve();
            diagnostics.RunFunctionOnDifferentSizeArrays(eratosthenesBasic.GetPrimes);

            Console.WriteLine("EratosthenesSieveOnlyLessThanSqrt");
            var eratosthenesUpToSqrt = new EratosthenesSieveOnlyLessThanSqrt();
            diagnostics.RunFunctionOnDifferentSizeArrays(eratosthenesUpToSqrt.GetPrimes);

            Console.WriteLine("EratosthenesSieveOnlyOdds");
            var eratosthenesOnlyOdds = new EratosthenesSieveOnlyOdds();
            diagnostics.RunFunctionOnDifferentSizeArrays(eratosthenesOnlyOdds.GetPrimes);

            Console.WriteLine("AtkinSieve");
            var atkinBasic = new AtkinSieve();
            diagnostics.RunFunctionOnDifferentSizeArrays(atkinBasic.GetPrimes);

            Console.WriteLine("SunduramSieve");
            var sunduramBasic = new SundaramSieve();
            diagnostics.RunFunctionOnDifferentSizeArrays(sunduramBasic.GetPrimes);

            Console.ReadLine();
        }
    }
}
