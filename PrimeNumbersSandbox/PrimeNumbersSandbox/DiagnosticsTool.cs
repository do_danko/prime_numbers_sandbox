﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace PrimeNumbersSandbox
{
    public class DiagnosticsTool
    {
        private readonly int[] arraySizesToTest = {
            100
            //,250, 500
            //,1000, 2500, 5000
            //,10000, 25000, 50000
            //,100000, 250000, 500000
            //,1000000, 2500000, 5000000
            //,10000000, 25000000, 50000000
            //,100000000, 250000000, 500000000
            //,1000000000, int.MaxValue
        };

        public void RunFunctionOnDifferentSizeArrays<T>(Func<ulong, IEnumerable<T>> functionToTest)
        {
            var stopWatch = new Stopwatch();

            foreach (var arraySize in arraySizesToTest)
            {
                var ulongSize = (ulong)arraySize;
                stopWatch.Start();
                var result = functionToTest(ulongSize);
                stopWatch.Stop();

                OutputResult(arraySize, stopWatch.ElapsedMilliseconds, result);
                stopWatch.Reset();
            }
        }

        private void OutputResult<T>(int arraySize, double elapsedTimeMs, IEnumerable<T> result)
        {
            Console.WriteLine($"Testing arraySize: {arraySize}");
            Console.WriteLine($"Elapsed time: {elapsedTimeMs} ms");
            Console.WriteLine(string.Join(",", result));
        }
    }
}
