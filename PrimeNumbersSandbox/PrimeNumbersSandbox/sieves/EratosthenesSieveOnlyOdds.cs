﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrimeNumbersSandbox
{
    public class EratosthenesSieveOnlyOdds : IPrimesRetriever
    {
        public List<ulong> GetPrimes(ulong ceiling)
        {
            if (ceiling < 0)
            {
                throw new ArgumentException();
            }

            if (ceiling <= int.MaxValue)
            {
                var newCeiling = Convert.ToInt32(Math.Ceiling(ceiling / 2.0));
                
                var primes = new bool[newCeiling];
                primes.Populate(true);
                primes[0] = false;

                for (int i = 1; i < newCeiling; ++i)
                {
                    var step = 2 * i + 1;
                    for (int j = i + step; j < newCeiling; j += step)
                    {
                        primes[j] = false;
                    }
                }

                var result = primes.GetIndexes(x => x)
                    .Select(x => 2 * x + 1)
                    .Select(x => (ulong)x)
                    .ToList();

                result.Add(2);

                return result;
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
