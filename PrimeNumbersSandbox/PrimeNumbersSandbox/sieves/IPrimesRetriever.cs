﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrimeNumbersSandbox
{
    public interface IPrimesRetriever
    {
        List<ulong> GetPrimes(ulong ceiling);
    }
}
