﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrimeNumbersSandbox
{
    public class AtkinSieve : IPrimesRetriever
    {
        public List<ulong> GetPrimes(ulong ceiling)
        {
            if (ceiling < 0)
            {
                throw new ArgumentException();
            }

            if (ceiling <= int.MaxValue)
            {
                var narrowedToIntCeiling = (int)ceiling;

                var primes = new bool[narrowedToIntCeiling];
                var ceilingSqrt = Math.Sqrt(narrowedToIntCeiling);

                for (int x = 1; x <= ceilingSqrt; ++x)
                {
                    for (int y = 1; y <= ceilingSqrt; ++y)
                    {
                        var x2 = x * x;
                        var y2 = y * y;

                        var n = 4 * x2 + y2;
                        if (n <= narrowedToIntCeiling 
                            && (n % 12 == 1 || n % 12 == 5))
                        {
                            primes[n] ^= true;
                        }

                        n = 3 * x2 + y2;
                        if (n <= narrowedToIntCeiling 
                            && n % 12 == 7)
                        {
                            primes[n] ^= true;
                        }

                        n = 3 * x2 - y2;
                        if (x > y 
                            && n <= narrowedToIntCeiling && n % 12 == 11)
                        {
                            primes[n] ^= true;
                        }
                    }
                }

                for (int i = 5; i <= ceilingSqrt; i += 2)
                {
                    if (primes[i])
                    {
                        var square = i * i;
                        for (int j = square; j < narrowedToIntCeiling; j += square)
                        {
                            primes[j] = false;
                        }
                    }
                }

                primes[2] = true;
                primes[3] = true;

                var result = primes.GetIndexes(x => x)
                    .Select(x => (ulong)x)
                    .ToList();

                return result;
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
