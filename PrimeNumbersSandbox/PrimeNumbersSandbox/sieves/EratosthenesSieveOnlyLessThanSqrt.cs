﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrimeNumbersSandbox
{
    public class EratosthenesSieveOnlyLessThanSqrt : IPrimesRetriever
    {
        private const int MIN_PRIME = 2;

        public List<ulong> GetPrimes(ulong ceiling)
        {
            if (ceiling < 0)
            {
                throw new ArgumentException();
            }

            if (ceiling <= int.MaxValue)
            {
                var narrowedToIntCeiling = (int)ceiling;
                var primes = new bool[ceiling];

                primes.Populate(true);
                primes[0] = false;
                primes[1] = false;

                for (int i = MIN_PRIME; i * i < narrowedToIntCeiling; ++i)
                {
                    for (int j = i + i; j < narrowedToIntCeiling; j += i)
                    {
                        primes[j] = false;
                    }
                }

                var result = primes.GetIndexes(x => x)
                    .Select(x => (ulong)x)
                    .ToList();

                return result;
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
