﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PrimeNumbersSandbox
{
    public class SundaramSieve : IPrimesRetriever
    {
        public List<ulong> GetPrimes(ulong ceiling)
        {
            if (ceiling < 0)
            {
                throw new ArgumentException();
            }

            if (ceiling <= int.MaxValue)
            {
                var narrowedToIntCeiling = (int) ceiling;

                var halfCeiling = (int) Math.Ceiling(narrowedToIntCeiling / 2.0);

                var primes = new bool[halfCeiling];
                primes.Populate(true);
                primes[0] = false;

                for (int i = 1; i < halfCeiling; ++i)
                {
                    for (int j = 1; j < halfCeiling; ++j)
                    {
                        var n = 2 * i * j + i + j;
                        if (halfCeiling <= n)
                        {
                            break;
                        }

                        primes[n] = false;
                    }
                }

                var result = primes.GetIndexes(x => x)
                    .Select(x => 2 * x + 1)
                    .Select(x => (ulong)x)
                    .ToList();

                result.Add(2);

                return result;
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
